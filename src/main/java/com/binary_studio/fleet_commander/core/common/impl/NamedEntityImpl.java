package com.binary_studio.fleet_commander.core.common.impl;

import com.binary_studio.fleet_commander.core.common.NamedEntity;

public class NamedEntityImpl implements NamedEntity {

	private final String name;

	public NamedEntityImpl(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
