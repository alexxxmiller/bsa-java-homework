package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.common.impl.NamedEntityImpl;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private final DefenciveSubsystem defenciveSubsystem;

	private final AttackSubsystem attackSubsystem;

	private final PositiveInteger originalShieldHP;

	private final PositiveInteger originalHullHP;

	private final PositiveInteger originalCapacitorAmount;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitorAmount;

	private boolean isTurned;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size, DefenciveSubsystem defenciveSubsystem, AttackSubsystem attackSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.defenciveSubsystem = defenciveSubsystem;
		this.attackSubsystem = attackSubsystem;
		this.originalShieldHP = shieldHP;
		this.originalHullHP = hullHP;
		this.originalCapacitorAmount = capacitorAmount;
	}

	@Override
	public void endTurn() {
		if (this.originalCapacitorAmount.value() > this.capacitorAmount.value()) {
			int recharge = this.capacitorAmount.value() + this.capacitorRechargeRate.value();
			if (recharge > this.originalCapacitorAmount.value()) {
				this.capacitorAmount = this.originalCapacitorAmount;
			}
			else {
				this.capacitorAmount = PositiveInteger.of(recharge);
			}
		}
		this.isTurned = false;
	}

	@Override
	public void startTurn() {
		this.isTurned = true;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.isTurned && this.capacitorAmount.value() >= this.attackSubsystem.getCapacitorConsumption().value()) {
			AttackAction action = new AttackAction(this.attackSubsystem.attack(target), new NamedEntityImpl(this.name),
					new NamedEntityImpl(target.getName()), new NamedEntityImpl(this.attackSubsystem.getName()));
			this.capacitorAmount = PositiveInteger.of(0);
			return Optional.of(action);
		}

		return Optional.empty();
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction action = this.defenciveSubsystem.reduceDamage(attack);
		int damage;

		if (this.shieldHP.value() > 0) {
			damage = this.shieldHP.value() - action.damage.value();
			this.shieldHP = PositiveInteger.of(Math.max(damage, 0));
		}
		else {
			damage = this.hullHP.value() - action.damage.value();
			this.hullHP = PositiveInteger.of(Math.max(damage, 0));
			if (this.hullHP.value() <= 0) {
				return new AttackResult.Destroyed();
			}
		}

		return new AttackResult.DamageRecived(action.weapon, action.damage, action.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int shieldHPRegenerated;
		int hullHPRegenerated;

		if (this.isTurned
				&& this.capacitorAmount.value() >= this.defenciveSubsystem.getCapacitorConsumption().value()) {
			if (this.shieldHP.value().equals(this.originalShieldHP.value())) {
				shieldHPRegenerated = 0;
			}
			else {
				shieldHPRegenerated = this.defenciveSubsystem.regenerate().shieldHPRegenerated.value();
				if (shieldHPRegenerated > this.originalShieldHP.value()) {
					this.shieldHP = PositiveInteger.of(this.originalShieldHP.value());
				}
				else {
					this.shieldHP = PositiveInteger.of(shieldHPRegenerated + this.shieldHP.value());
				}
			}

			if (this.hullHP.value().equals(this.originalHullHP.value())) {
				hullHPRegenerated = 0;
			}
			else {
				hullHPRegenerated = this.hullHP.value()
						+ this.defenciveSubsystem.regenerate().hullHPRegenerated.value();
				if (hullHPRegenerated > this.originalShieldHP.value()) {
					this.hullHP = PositiveInteger.of(this.originalHullHP.value());
				}
				else {
					this.hullHP = PositiveInteger.of(hullHPRegenerated);
				}
			}

			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());

			return Optional.of(new RegenerateAction(PositiveInteger.of(shieldHPRegenerated),
					PositiveInteger.of(hullHPRegenerated)));
		}

		return Optional.empty();
	}

}
