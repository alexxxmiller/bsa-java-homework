package com.binary_studio.uniq_in_sorted_stream;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		Set<Long> values = new HashSet<>();
		return stream.filter(e -> values.add(e.getPrimaryId()));
	}

}
