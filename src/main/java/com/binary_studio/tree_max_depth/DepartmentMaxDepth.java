package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		Queue<Department> departments = new LinkedList<>(rootDepartment.subDepartments);
		int depth = 1;

		while (true) {
			int nodeCount = departments.size();
			if (nodeCount == 0) {
				return depth;
			}
			else {
				depth++;
			}

			while (nodeCount > 0) {
				Department peekedDep = departments.peek();
				departments.poll();

				if (peekedDep != null && !peekedDep.subDepartments.isEmpty()) {
					List<Department> subDepartment = peekedDep.subDepartments.stream().filter(Objects::nonNull)
							.collect(Collectors.toList());
					departments.addAll(subDepartment);
				}
				nodeCount--;
			}
		}
	}

}
